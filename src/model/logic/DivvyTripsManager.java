package model.logic;

import java.io.FileReader;
import java.util.Iterator;

import com.opencsv.CSVReader;

import api.IDivvyTripsManager;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.DoubleLinkedList;
import model.data_structures.DoublyLinkedList;

public class DivvyTripsManager implements IDivvyTripsManager {

	//-----------------------------------------------------------
	// Atributos
	//-----------------------------------------------------------

	private DoubleLinkedList<VOStation> stations;

	private DoubleLinkedList<VOTrip> trips;

	//-----------------------------------------------------------
	// Constructor
	//-----------------------------------------------------------
	
	public DivvyTripsManager() {
		this.stations = new DoubleLinkedList<>();
		this.trips = new DoubleLinkedList<>();
	}

	//-----------------------------------------------------------
	// Métodos
	//-----------------------------------------------------------

	public void loadStations (String stationsFile) {
		try {
			CSVReader reader = new CSVReader(new FileReader(stationsFile));
			String[] nextLine;
			reader.readNext();
			
			while((nextLine = reader.readNext()) != null) {
				VOStation station = new VOStation(Integer.parseInt(nextLine[0]), 
						nextLine[1], nextLine[2], Double.parseDouble(nextLine[3]), 
						Double.parseDouble(nextLine[4]), Integer.parseInt(nextLine[5]), nextLine[6]);

				stations.addAtFirst(station);
			}
			
			reader.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	public void loadTrips (String tripsFile) {
		try {
			CSVReader reader = new CSVReader(new FileReader(tripsFile));
			String[] nextLine;
			reader.readNext();
			
			while((nextLine = reader.readNext()) != null) {
				VOTrip trip = null;
				
				if(nextLine[10].isEmpty() && nextLine[11].isEmpty()) {
					trip = new VOTrip(Integer.parseInt(nextLine[0]), nextLine[1], nextLine[2], 
							Integer.parseInt(nextLine[3]), Integer.parseInt(nextLine[4]), 
							Integer.parseInt(nextLine[5]), nextLine[6], Integer.parseInt(nextLine[7]), 
							nextLine[8], nextLine[9], "", 0);
				
				} else if (nextLine[11].isEmpty()) {
					trip = new VOTrip(Integer.parseInt(nextLine[0]), nextLine[1], nextLine[2], 
							Integer.parseInt(nextLine[3]), Integer.parseInt(nextLine[4]), 
							Integer.parseInt(nextLine[5]), nextLine[6], Integer.parseInt(nextLine[7]), 
							nextLine[8], nextLine[9], nextLine[10], 0);
					
				} else {
					trip = new VOTrip(Integer.parseInt(nextLine[0]), nextLine[1], nextLine[2], 
							Integer.parseInt(nextLine[3]), Integer.parseInt(nextLine[4]), 
							Integer.parseInt(nextLine[5]), nextLine[6], Integer.parseInt(nextLine[7]), 
							nextLine[8], nextLine[9], nextLine[10], Integer.parseInt(nextLine[11]));
				}

				trips.addAtFirst(trip);
			}
			
			reader.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

	}

	@Override
	public DoublyLinkedList <VOTrip> getTripsOfGender (String gender) {
		DoubleLinkedList<VOTrip> genderTrips = new DoubleLinkedList<>();
		Iterator<VOTrip> iter = trips.iterator();

		while(iter.hasNext()) {
			VOTrip trip = iter.next();

			if(trip.getGender().equals(gender)) {
				genderTrips.addAtFirst(trip);
			}
		}

		return genderTrips;
	}

	@Override
	public DoublyLinkedList <VOTrip> getTripsToStation (int stationID) {
		DoubleLinkedList<VOTrip> tripsStation = new DoubleLinkedList<>();
		Iterator<VOTrip> iter = trips.iterator();
		
		while(iter.hasNext()) {
			VOTrip trip = iter.next();
			
			if(trip.getToStationId() == stationID) {
				tripsStation.addAtFirst(trip);
			}
		}
		
		return tripsStation;
	}	


}
