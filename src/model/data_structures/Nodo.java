package model.data_structures;

public class Nodo<T> {

	//-----------------------------------------------------------
	// Atributos
	//-----------------------------------------------------------

	private T element;

	private Nodo<T> previous;

	private Nodo<T> next;
	
	//-----------------------------------------------------------
	// Constructor
	//-----------------------------------------------------------

	/**
	 * @param element
	 */
	public Nodo(T element) {
		this.element = element;
		this.previous = null;
		this.next = null;
	}
	
	//-----------------------------------------------------------
	// Constructor
	//-----------------------------------------------------------

	/**
	 * @return the element
	 */
	public T getElement() {
		return element;
	}

	/**
	 * @param element the element to set
	 */
	public void setElement(T element) {
		this.element = element;
	}

	/**
	 * @return the previous element.
	 */
	public Nodo<T> getPrevious() {
		return previous;
	}

	/**
	 * @param previous the previous to set
	 */
	public void setPrevious(Nodo<T> previous) {
		this.previous = previous;
	}

	/**
	 * @return the next element.
	 */
	public Nodo<T> getNext() {
		return next;
	}

	/**
	 * @param next the next to set
	 */
	public void setNext(Nodo<T> next) {
		this.next = next;
	}

	
	
	

}
