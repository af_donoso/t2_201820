package model.data_structures;

import java.util.Iterator;

public class DoubleLinkedList<T extends Comparable<T>> implements DoublyLinkedList<T> {

	//-----------------------------------------------------------
	// Atributos
	//-----------------------------------------------------------

	private Nodo<T> firstNode;

	private int listSize;

	//-----------------------------------------------------------
	// Constructor
	//-----------------------------------------------------------

	public DoubleLinkedList() {
		firstNode = null;
		listSize = 0;
	}

	public DoubleLinkedList(T element) {
		firstNode = new Nodo<T>(element);
		listSize = 1;
	}

	//-----------------------------------------------------------
	// Métodos
	//-----------------------------------------------------------

	@Override
	public Iterator<T> iterator() {

		return new Iterator<T>() {
			Nodo<T> act = null;

			@Override
			public boolean hasNext() {
				if (listSize == 0) {
					return false;
				}

				if (act == null) {
					return true;
				}

				return act.getNext() != null;

			}

			@Override
			public T next() {

				if(act == null) {
					act = firstNode;
				} else {
					act = act.getNext();
				}

				return act.getElement();
			}
		};
	}

	@Override
	public boolean add(T element) {
		boolean added = false;
		Nodo<T> node = new Nodo<T>(element);

		if(firstNode == null) {
			firstNode = node;

			added = true;
		} else {
			try {
				Nodo<T> actual = firstNode;

				while(actual.getNext() != null) {
					actual = actual.getNext();
				}

				actual.setNext(node);
				node.setPrevious(actual);

				added = true;
			} catch (Exception e) {

			}
		}

		if (added) listSize ++;

		return added;
	}

	@Override
	public boolean addAtK(T element, int k) {
		boolean added = false;
		try {
			Nodo<T> nodo = new Nodo<T>(element);

			Nodo<T> actual = firstNode;
			for(int i = 0; i < k - 1; i++) {
				actual = actual.getNext();
			}

			Nodo<T> siguiente = actual.getNext();

			siguiente.setPrevious(nodo);
			nodo.setNext(siguiente);
			nodo.setPrevious(actual);
			actual.setNext(nodo);
		} catch (Exception e) {

		}

		if(added) listSize ++;

		return added;
	}

	@Override
	public boolean addAtFirst(T element) {
		boolean added = false;

		try {
			Nodo<T> node = new Nodo<T>(element);

			if(firstNode == null) {
				firstNode = node;
			} else {
				firstNode.setPrevious(node);
				node.setNext(firstNode);
				firstNode = node;
			}

			added = true;
		} catch (Exception e) {

		}

		if (added) listSize ++;

		return added;
	}

	@Override
	public Integer getSize() {
		return listSize;
	}

	@Override
	public boolean delete(T element) {
		boolean deleted = false;

		try {

			if (firstNode.getElement().equals(element)) {
				firstNode = firstNode.getNext();
				firstNode.setPrevious(null);

				deleted = true;

			} else {
				Nodo<T> actual = firstNode;

				while(actual != null && !deleted) {
					if (actual.getElement().equals(element) && actual.getNext() == null) {
						Nodo<T> previous = actual.getPrevious();
						previous.setNext(null);

						deleted = true;

					} else if (actual.getElement().equals(element)) {
						Nodo<T> previous = actual.getPrevious();
						Nodo<T> next = actual.getNext();

						previous.setNext(next);
						next.setPrevious(previous);

						deleted = true;
					}
				}
			}

		} catch (Exception e) {

		}

		if (deleted) listSize --;

		return deleted;
	}

	@Override
	public boolean deleteAtK(int k) {
		boolean deleted = false;

		try {
			if (k == 0) {
				firstNode = firstNode.getNext();
				firstNode.setPrevious(null);

				deleted = true;
			} else {
				Nodo<T> actual = firstNode;

				for(int i = 0; i < k; i++) {
					actual = actual.getNext();
				}

				if (actual.getNext() != null) {
					Nodo<T> previous = actual.getPrevious();
					Nodo<T> next = actual.getNext();

					previous.setNext(next);
					next.setPrevious(previous);

					deleted = true;
				} else {
					Nodo<T> previous = actual.getPrevious();
					previous.setNext(null);

					deleted = true;
				}
			}

		} catch (Exception e) {

		}

		if (deleted) listSize --;

		return deleted;
	}

	@Override
	public T getElement(int k) {
		Nodo<T> actual = firstNode;

		for(int i = 0; i < k; i++) {
			actual = actual.getNext();
		}

		return actual.getElement();
	}

}
