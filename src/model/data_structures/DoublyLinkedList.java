package model.data_structures;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface DoublyLinkedList<T extends Comparable<T>> extends Iterable<T>{
	
	boolean add(T element);
	
	boolean addAtK(T element, int k);
	
	boolean addAtFirst(T element);
	
	T getElement(int k);

	Integer getSize();
	
	boolean delete(T element);
	
	boolean deleteAtK(int k);
	
	
}
